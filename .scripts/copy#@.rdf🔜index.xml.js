#!/usr/bin/env -S deno run --allow-read --allow-write

processingPath: for ( const path of Deno.args ) {
	try {
		const children = Array.from(Deno.readDirSync(path))
		for ( const possibleFileName of
			[ "index.xml"
			, "index.xhtml"
			, "index.html" ] ) {
			try {
				Deno.statSync(`${ path }/${ possibleFileName }`)
				continue processingPath }
			catch ( error ) {
				if ( !(error instanceof Deno.errors.NotFound) )
					throw error
				else continue } }
		Deno.copyFileSync(`${ path }/@.rdf`, `${ path }/index.xml`) }
	catch { continue processingPath } }
