#!/usr/bin/env -S deno run --allow-read --allow-write
import { expandGlobSync } from "./deps.js"

const x·m·lIndices = [ ]
for ( const { path } of expandGlobSync("./**/index.{xhtml,xml}") ) {
	try {
		Deno.statSync(path.replace(/\.x(?:ht)?ml$/u, ".html"))
		continue }
	catch ( error ) {
		if ( !(error instanceof Deno.errors.NotFound) )
			throw error
		else x·m·lIndices.push(path.replace(Deno.cwd(), "")) } }
Deno.writeTextFileSync("_redirects", x·m·lIndices.map($ => `${ $.substring(0, $.lastIndexOf("/") + 1) } ${ $ } 200
`).join(""))
