#!/usr/bin/env -S deno run --allow-read --allow-write --allow-run

await Promise.all
  ( Deno.args.map
      ( $ => {
          const module = Deno.realPathSync($)
          const outputPath= module
            .replace(Deno.cwd(), "./.public-build")
            .replace(/(?<=\.\/\.public-build\/)\./u, "")
          Deno.mkdirSync
            ( outputPath.substr(0, outputPath.lastIndexOf("/"))
            , { recursive: true } )
          console.log(module, outputPath)
          return Deno
            .run({ cmd: [ "deno", "bundle", module, outputPath ] })
            .status()
        } ) )
