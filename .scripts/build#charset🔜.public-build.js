#!/usr/bin/env -S deno run --allow-read --allow-write
import { expandGlobSync } from "./deps.js"

const processFile = ( file, path ) =>
	file
		.trim()
		.split(/[\n\u2028]/u)
		.flatMap(line => {
			const includeMatch = /^\/\/=\s*include\s*(?:"([^"]*)"|'([^']*)')\s*$/u.exec(line)
				, includePath = includeMatch != null ? includeMatch[1] ?? includeMatch[2] : null
			return includePath
				? Array
					.from(expandGlobSync(`${ path }/../${ includePath }`))
					.map(file => processFile(Deno.readTextFileSync(file.path), file.path))
					.join("\u2028\u2028")
				: [ line ] })
		.join("\u2028")

for ( const path of Deno.args.map($ => Deno.realPathSync($)) ) {
	const outputPath = path.replace(Deno.cwd(), "./.public-build").replace(/(?<=\.\/\.public-build\/)\.|\/$/gu, "")
	Deno.mkdirSync(
		outputPath.substr(0, outputPath.lastIndexOf("/")),
		{ recursive: true })
	Deno.writeTextFileSync(
		outputPath,
		processFile(
			Deno.readTextFileSync(`${ path }/index.kch`),
			`${ path }/index.kch`)) }
