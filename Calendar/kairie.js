export default class {
  /** Ecmascript Date object. */
  #date;
  /** Whether the Gregorian date comes after the Kairie new year. */
  get #isAfterKNewYear() {
    const date = this.#date;
    const gMonth = date.getMonth() + 1;
    return gMonth > 9 || gMonth == 9 && date.getDate() >= 22;
  }

  constructor(date = Date.now()) {
    this.#date = new Date(date);
  }

  /** Current day in Kairie tide (1–120) */
  get dayInTide() {
    return this.dayInYear % 120 || 120;
  }

  /** Current day in Kairie tierce (1–40) */
  get dayInTierce() {
    return this.dayInYear % 40 || 40;
  }

  /** Current day in Kairie week (1–10) */
  get dayInWeek() {
    return this.dayInYear % 10 || 10;
  }

  /** Current day in Kairie year (1–366) */
  get dayInYear() {
    const date = this.#date;
    const currentYear = date.getFullYear();
    const referenceYear = currentYear - !this.#isAfterKNewYear;
    return Math.round(
      (
        Date.UTC(
          currentYear,
          date.getMonth(),
          date.getDate(),
        ) - Date.UTC(referenceYear, 8, /* not 9 */ 21)
      ) / 86_400_000,
    );
  }

  /** Current Kairie tide (1–4). */
  get tideInYear() {
    return Math.ceil(this.dayInYear / 120);
  }

  /** Current Kairie tierce in the current tide (1–3). */
  get tierceInTide() {
    return Math.ceil(this.dayInTide / 40);
  }

  /** Current Kairie week in the current tide (1–12). */
  get weekInTide() {
    return Math.ceil(this.dayInTide / 10);
  }

  /** Current Kairie week in the current tierce (1–4). */
  get weekInTierce() {
    return Math.ceil(this.dayInTierce / 10);
  }

  /** Current Kairie week in the current year (1–37). */
  get weekInYear() {
    return Math.ceil(this.dayInYear / 10);
  }

  /**
   * Current Kairie year, roughly equivalent to current Gregorian year
   * minus two thousand.
   *
   * This year advances on 22 September.
   */
  get yearInEra() {
    const date = this.#date;
    return date.getFullYear() - 2000 + this.#isAfterKNewYear;
  }
}
